# frozen_string_literal: true

class RefreshConventionSearchesMaterializedViewJob < ApplicationJob
  queue_as :default

  def perform
    Convention.refresh_materialized_view
  end
end