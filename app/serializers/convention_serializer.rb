# frozen_string_literal: true

class ConventionSerializer < Panko::Serializer
  attributes :id, :name, :start_date, :end_date, :created_at, :updated_at
end