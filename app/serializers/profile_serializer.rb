# frozen_string_literal: true

class ProfileSerializer < Panko::Serializer
  attributes :id, :name, :created_at, :updated_at

  has_many :public_conventions, each_serializer: ConventionSerializer, name: :attended_conventions
end