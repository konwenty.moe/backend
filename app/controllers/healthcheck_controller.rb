# frozen_string_literal: true

class HealthcheckController < ActionController::API
  def up
    head :ok
  end
end
