# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Dry::Monads[:result]

  after_action :set_jwt_token

  def set_jwt_token
    return unless rodauth.use_jwt? && rodauth.valid_jwt?

    response.headers["Authorization"] = rodauth.session_jwt
  end

  unless Rails.env.production?
    around_action :n_plus_one_detection

    def n_plus_one_detection
      Prosopite.scan
      yield
    ensure
      Prosopite.finish
    end
  end

  private

  def authenticate!
    rodauth.require_account
  end

  def current_account
    rodauth.rails_account
  end
  alias :current_user :current_account
end