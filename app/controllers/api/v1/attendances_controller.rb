# frozen_string_literal: true

module Api
  module V1
    class AttendancesController < Api::V1::ApplicationController
      def create
        service = Attendances::New.new

        case service.call(params.to_unsafe_hash)
        in Success(attendance)
          render json: attendance, status: :created
        in Failure(errors)
          render json: errors, status: :unprocessable_entity
        end
      end
    end
  end
end