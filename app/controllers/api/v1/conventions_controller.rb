# frozen_string_literal: true

module Api
  module V1
    class ConventionsController < Api::V1::ApplicationController
      before_action :authenticate!, only: %i[create update destroy]

      def index
        service = Conventions::List.new

        case service.call
        in Success(coventions)
          render json: coventions, status: :ok
        in Failure(errors)
          render json: errors, status: :unprocessable_entity
        end
      end

      def search
        service = Conventions::Search.new

        case service.call(params.to_unsafe_hash)
        in Success(conventions)
          render json: conventions, status: :ok
        in Failure(errors)
          render json: errors, status: :unprocessable_entity
        end
      end

      def show
        service = Conventions::Show.new

        case service.call(params.to_unsafe_hash)
        in Success(convention)
          render json: convention, status: :ok
        in Failure(errors)
          case errors
          in :not_found
            head :not_found
          else
            render json: errors, status: :unprocessable_entity
          end
        end
      end

      def create
        service = Conventions::New.new

        service_params = [
          current_user,
          params.to_unsafe_hash
        ]

        case service.call(service_params)
        in Success(covention)
          render json: covention, status: :created
        in Failure(errors)
          render json: errors, status: :unprocessable_entity
        end
      end

      def update
        service = Conventions::Update.new
        service_params = [
          current_user,
          params.to_unsafe_hash
        ]

        case service.call(service_params)
        in Success(convention)
          render json: convention, status: :ok
        in Failure(errors)
          case errors
          in :unauthorized
            head :forbidden
          in :not_found
            head :not_found
          else
            render json: errors, status: :unprocessable_entity
          end
        end
      end

      def destroy
        service = Conventions::Delete.new
        service_params = [
          current_user,
          params.to_unsafe_hash
        ]

        case service.call(service_params)
        in Success
          head :ok
        in Failure(errors)
          case errors
          in :unauthorized
            head :forbidden
          in :not_found
            head :not_found
          else
            head :bad_request
          end
        end
      end
    end
  end
end