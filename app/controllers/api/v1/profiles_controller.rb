# frozen_string_literal: true

module Api
  module V1
    class ProfilesController < Api::V1::ApplicationController
      def show
        service = Profiles::Show.new

        case service.call(params.to_unsafe_hash)
        in Success(profile)
          render json: profile, status: :ok
        in Failure(errors)
          case errors
          in :not_found
            head :not_found
          else
            render json: errors, status: :unprocessable_entity
          end
        end
      end
    end
  end
end