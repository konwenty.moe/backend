# frozen_string_literal: true

module Api
  module V1
    class ApplicationController < ::ApplicationController
      rescue_from ActionDispatch::Http::Parameters::ParseError do |_exception|
        head :bad_request
      end
    end
  end
end