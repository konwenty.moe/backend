# frozen_string_literal: true

class BaseService
  include Dry::Transaction
  include Pundit::Authorization
end