# frozen_string_literal: true

module Profiles
  class Show < BaseService
    step :validate
    step :find
    step :serialize

    private

    def validate(input)
      contract.call(input).to_monad.or { |validation_result| Failure(validation_result.errors.to_h) }
    end

    def contract
      Profiles::ShowContract.new
    end

    def find(input)
      params = input.to_h
      profile = Profile.find(params[:id])
      Success(profile)
    rescue ActiveRecord::RecordNotFound
      Failure(:not_found)
    end

    def serialize(profile)
      Success(
        ::ProfileSerializer.new.serialize(profile)
      )
    end
  end
end