# frozen_string_literal: true

module Attendances
  class New < BaseService
    step :validate
    step :create

    private

    def validate(input)
      contract.call(input).to_monad
              .or { |validation_result| Failure(validation_result.errors.to_h) }
    end

    def contract
      Attendances::NewContract.new
    end

    def create(input)
      params = input.to_h
      attendance = Attendance.new(params)

      case attendance.save
      when true
        Success(attendance)
      else
        Failure(attendance.errors.to_hash)
      end
    end
  end
end