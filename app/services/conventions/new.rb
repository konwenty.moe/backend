# frozen_string_literal: true

module Conventions
  class New < BaseService
    step :authorization
    step :validate
    step :create

    private

    def contract
      Conventions::NewContract.new
    end

    attr_reader :current_user

    def authorization(user_and_params)
      @current_user, params = user_and_params
      authorize Convention, :new?
      Success(params)
    rescue Pundit::NotAuthorizedError
      Failure(:unauthorized)
    end

    def validate(params)
      contract.call(params).to_monad
              .fmap(&:to_h)
              .or { |validation_result| Failure(validation_result.errors.to_h) }
    end

    def create(params)
      convention = Convention.new(params.to_h)
      convention.created_by = current_user

      case convention.save
      when true
        Success(convention)
      else
        Failure(convention.errors.to_hash)
      end
    end
  end
end