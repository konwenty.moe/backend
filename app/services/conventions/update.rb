# frozen_string_literal: true

module Conventions
  class Update < BaseService
    step :set_current_user
    step :validate
    step :find_convention
    step :authorization
    step :update

    private

    attr_reader :current_user

    def contract
      Conventions::UpdateContract.new
    end

    def set_current_user(user_and_params) # rubocop:disable Naming/AccessorMethodName
      @current_user, params = user_and_params
      Success(params)
    end

    def validate(params)
      contract.call(params).to_monad
              .fmap(&:to_h)
              .or { |validation_result| Failure(validation_result.errors.to_h) }
    end

    def find_convention(params)
      convention = Convention.find_by(id: params[:id])

      return Failure(:not_found) unless convention

      Success([convention, params])
    end

    def authorization(convention_and_params)
      convention, params = convention_and_params
      authorize convention, :update?
      Success([convention, params])
    rescue Pundit::NotAuthorizedError
      Failure(:unauthorized)
    end

    def update(convention_and_params)
      convention, params = convention_and_params

      case convention.update(params)
      when true
        Success(convention)
      else
        Failure(convention.errors.to_hash)
      end
    end
  end
end