# frozen_string_literal: true

module Conventions
  class Delete < BaseService
    step :find
    step :authorization
    step :delete

    private

    attr_reader :current_user

    def find(user_and_params)
      user, params = user_and_params
      id = params["id"]
      convention = Convention.find_by(id:)
      convention ? Success([user, convention]) : Failure(:not_found)
    end

    def authorization(user_and_convention)
      @current_user, convention = user_and_convention

      Success(authorize(convention, :destroy?))
    rescue Pundit::NotAuthorizedError
      Failure(:unauthorized)
    end

    def delete(convention)
      convention.destroy
      Success()
    end
  end
end