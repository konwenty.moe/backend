# frozen_string_literal: true

module Conventions
  class Show < BaseService
    step :validate
    step :find

    private

    def validate(input)
      contract.call(input).to_monad.or { |validation_result| Failure(validation_result.errors.to_h) }
    end

    def contract
      Conventions::ShowContract.new
    end

    def find(input)
      params = input.to_h
      convention = Convention.find(params[:id])
      Success(convention)
    rescue ActiveRecord::RecordNotFound
      Failure(:not_found)
    end
  end
end