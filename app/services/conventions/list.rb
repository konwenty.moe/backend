# frozen_string_literal: true

module Conventions
  class List < BaseService
    step :authenticate
    step :authorize
    step :fetch

    private

    def authenticate
      Success()
    end

    def authorize
      Success()
    end

    def fetch
      Success(
        Convention.reorder(start_date: :asc).all
      )
    end
  end
end