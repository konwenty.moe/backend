# frozen_string_literal: true

module Conventions
  class Search < BaseService
    step :validate
    step :search

    private

    def validate(input)
      contract.call(input).to_monad.or { |validation_result| Failure(validation_result.errors.to_h) }
    end

    def contract
      Conventions::SearchContract.new
    end

    def search(input)
      params = input.to_h
      conventions = Convention.full_text_search(params[:query])

      Success(conventions)
    end
  end
end