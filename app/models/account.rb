# frozen_string_literal: true

class Account < ApplicationRecord
  include Rodauth::Rails.model
  enum :status, unverified: 1, verified: 2, closed: 3
  has_one :profile, dependent: :destroy
  has_many :created_conventions, class_name: "Convention", foreign_key: "created_by_id", inverse_of: :created_by, dependent: :restrict_with_error
end