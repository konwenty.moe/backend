# frozen_string_literal: true

class Convention < ApplicationRecord
  validates :name, :city, :address, :start_date, :end_date, presence: true

  validates :socials, json_schema: { filename: "socials" }
  attribute :socials, :ind_jsonb

  belongs_to :created_by, class_name: "Account", inverse_of: :created_conventions

  scope :full_text_search, ->(query) { where(id: ConventionSearch.search(query).select(:convention_id)) }

  def self.refresh_materialized_view
    Scenic.database.refresh_materialized_view(
      :convention_searches,
      concurrently: true,
    )
  end
end