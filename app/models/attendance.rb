# frozen_string_literal: true

class Attendance < ApplicationRecord
  belongs_to :convention
  belongs_to :profile

  enum visibility: {
    public: 1,
    private: 2,
    friends_only: 3
  }, _prefix: true

  enum role: {
    participant: 1,
    helper: 2,
    organizer: 3,
    guest: 4,
  }

  validates :visibility, :role, presence: true
end