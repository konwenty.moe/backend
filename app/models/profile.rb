# frozen_string_literal: true

class Profile < ApplicationRecord
  belongs_to :account
  has_many :attendances, dependent: :destroy
  has_many :conventions, through: :attendances
  has_many :public_attendances, -> { visibility_public }, class_name: "Attendance",
                                                          dependent: :destroy, inverse_of: :profile
  has_many :public_conventions, through: :public_attendances, source: :convention

  has_many :private_attendances, -> { visibility_private }, class_name: "Attendance",
                                                            dependent: :destroy, inverse_of: :profile
  has_many :private_conventions, through: :private_attendances, source: :convention
end