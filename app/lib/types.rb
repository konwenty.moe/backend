# frozen_string_literal: true

module Types
  include Dry.Types()

  VisibilityType = Types::String.default("public").enum(*Attendance.visibilities.keys)
  RoleType = Types::String.default("participant").enum(*Attendance.roles.keys)
end