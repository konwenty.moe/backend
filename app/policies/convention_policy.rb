# frozen_string_literal: true

class ConventionPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def update?
    record.created_by == user || user.admin?
  end

  def destroy?
    record.created_by == user || user.admin?
  end
end