# frozen_string_literal: true

class RodauthApp < Rodauth::Rails::App
  configure RodauthMain

  route do |r|
    next if r.path.start_with?("/up")

    r.rodauth
  end
end