# frozen_string_literal: true

module Attendances
  class NewContract < BaseContract
    json do
      required(:profile_id).filled(:integer)
      required(:convention_id).filled(:integer)
      required(:visibility).filled(::Types::VisibilityType)
      required(:role).filled(::Types::RoleType)
    end
  end
end