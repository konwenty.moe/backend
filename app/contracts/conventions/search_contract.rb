# frozen_string_literal: true

module Conventions
  class SearchContract < BaseContract
    params do
      required(:query).filled(:string)
    end
  end
end