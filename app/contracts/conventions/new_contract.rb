# frozen_string_literal: true

module Conventions
  class NewContract < BaseContract
    json do
      required(:name).filled(:string)
      required(:city).filled(:string)
      required(:address).filled(:string)
      required(:start_date).value(Types::JSON::DateTime)
      required(:end_date).value(Types::JSON::DateTime)
      optional(:socials).array(:hash) do
        required(:name).filled(:string)
        required(:url).filled(:string)
      end
    end

    rule(:start_date, :end_date) do
      key.failure("must be before end date") if values[:start_date] > values[:end_date]
    end

    rule(:end_date, :start_date) do
      key.failure("must be after start date") if values[:end_date] < values[:start_date]
    end
  end
end