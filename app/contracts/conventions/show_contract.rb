# frozen_string_literal: true

module Conventions
  class ShowContract < BaseContract
    params do
      required(:id).filled(:integer)
    end
  end
end