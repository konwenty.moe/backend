# frozen_string_literal: true

module Conventions
  class UpdateContract < BaseContract
    json do
      required(:id).value(Types::Params::Integer)
      optional(:name).filled(:string)
      optional(:city).filled(:string)
      optional(:address).filled(:string)
      optional(:start_date).value(Types::JSON::DateTime)
      optional(:end_date).value(Types::JSON::DateTime)
      optional(:socials).array(:hash) do
        required(:name).filled(:string)
        required(:url).filled(:string)
      end
    end

    rule(:start_date, :end_date) do
      if (key?(:start_date) && key?(:end_date)) && (values[:start_date] > values[:end_date])
        key.failure("must be before end date")
      end
    end

    rule(:end_date, :start_date) do
      if (key?(:start_date) && key?(:end_date)) && (values[:end_date] < values[:start_date])
        key.failure("must be after start date")
      end
    end
  end
end