# frozen_string_literal: true

Dry::Validation.load_extensions(:monads, :predicates_as_macros)

class BaseContract < Dry::Validation::Contract
  import_predicates_as_macros
end