FROM gitpod/workspace-base
ENV MISE_VER=2024.5.26 RUBY_VERSION=3.3.2
RUN sudo install-packages autoconf bison patch build-essential rustc libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libgmp-dev libncurses5-dev libffi-dev libgdbm6 libgdbm-dev libdb-dev uuid-dev libpq-dev
RUN sudo wget https://github.com/jdx/mise/releases/download/v${MISE_VER}/mise-v${MISE_VER}-linux-x64 -O /usr/local/bin/mise && \
    sudo chmod +x /usr/local/bin/mise && \
    echo 'eval "$(mise activate -s bash)"' >> ~/.bashrc && \
    mise install ruby@${RUBY_VERSION} && \
    MISE_DEBUG=1 mise exec ruby@${RUBY_VERSION} -- gem install bundler
COPY Gemfile Gemfile.lock ./
RUN MISE_DEBUG=1 mise exec ruby@${RUBY_VERSION} -- bundle install --jobs=$(nproc)