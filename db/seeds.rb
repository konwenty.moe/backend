# frozen_string_literal: true

account = Account.find_or_create_by(email: "admin@konwenty.moe") do |a|
  a.password_hash = "$argon2id$v=19$m=65536,t=2,p=1$if26hrCotwS0bq26+WYqDQ$t/RLeDV21dGw7k8usTmC9WwBlFM95w/MJT9K+/AHeXc"
  a.status = 2
end
Account.find_or_create_by(email: "ghost@konwenty.moe") do |a|
  a.password_hash = "$argon2id$v=19$m=65536,t=2,p=1$if26hrCotwS0bq26+WYqDQ$t/RLeDV21dGw7k8usTmC9WwBlFM95w/MJT9K+/AHeXc"
  a.status = 2
end
Profile.find_or_create_by(account_id: account.id)