SELECT
  conventions.id as convention_id,
  (
    to_tsvector('english', coalesce(conventions.name, '')) ||
    to_tsvector('english', unaccent(coalesce(conventions.name, ''))) ||
    to_tsvector('english', coalesce(conventions.description, '')) ||
    to_tsvector('english', unaccent(coalesce(conventions.description, ''))) ||
    to_tsvector('english', coalesce(conventions.city, '')) ||
    to_tsvector('english', unaccent(coalesce(conventions.city, '')))
  ) as tsv_document
FROM conventions