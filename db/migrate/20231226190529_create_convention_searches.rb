# frozen_string_literal: true

class CreateConventionSearches < ActiveRecord::Migration[7.0]
  def change
    create_view :convention_searches, materialized: true

    add_index :convention_searches, :convention_id, unique: true
    add_index :convention_searches, :tsv_document, using: :gin
  end
end
