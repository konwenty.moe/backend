# frozen_string_literal: true

class AddAddressToConvention < ActiveRecord::Migration[7.0]
  def change
    add_column :conventions, :address, :string, default: "", null: false
  end
end
