# frozen_string_literal: true

class CreateIndexOnConventionsStartDate < ActiveRecord::Migration[7.0]
  disable_ddl_transaction!

  INDEX_NAME = "idx_start_date_desc_on_conventions"

  def up
    add_index(
      :conventions,
      :start_date,
      name: INDEX_NAME,
      order: { start_date: :asc },
      if_not_exists: true,
      algorithm: :concurrently
    )
  end

  def down
    remove_index(
      :conventions,
      name: INDEX_NAME,
      algorithm: :concurrently
    )
  end
end
