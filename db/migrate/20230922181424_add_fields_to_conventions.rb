# frozen_string_literal: true

class AddFieldsToConventions < ActiveRecord::Migration[7.0]
  def change
    change_table :conventions, bulk: true do |t|
      t.column :location, :text, null: false, default: ""
      t.column :description, :text
      t.column :socials, :jsonb, null: false, default: []
    end
  end
end