# frozen_string_literal: true

class ChangeAttendancesVisibilityDefaults < ActiveRecord::Migration[7.0]
  VISIBILITY_PUBLIC = 1

  def up
    change_column_default :attendances, :visibility, VISIBILITY_PUBLIC
    change_column_null :attendances, :visibility, false
  end

  def down
    # no-op
  end
end