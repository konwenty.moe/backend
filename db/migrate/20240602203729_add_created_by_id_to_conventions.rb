# frozen_string_literal: true

class AddCreatedByIdToConventions < ActiveRecord::Migration[7.1]
  class Convention < ActiveRecord::Base; end # rubocop:disable Rails/ApplicationRecord

  def change
    Convention.delete_all
    add_column :conventions, :created_by_id, :bigint, null: false # rubocop:disable Rails/NotNullColumn
    add_foreign_key :conventions, :accounts, column: :created_by_id, on_delete: :restrict
  end
end