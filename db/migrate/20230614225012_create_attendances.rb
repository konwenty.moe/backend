# frozen_string_literal: true

class CreateAttendances < ActiveRecord::Migration[7.0]
  def change
    create_table :attendances do |t|
      t.references :convention, null: false, foreign_key: true
      t.references :profile, null: false, foreign_key: true
      t.integer :visibility

      t.timestamps
    end
  end
end
