# frozen_string_literal: true

class RenameLocationToCity < ActiveRecord::Migration[7.0]
  def change
    rename_column :conventions, :location, :city
  end
end