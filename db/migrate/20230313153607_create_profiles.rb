# frozen_string_literal: true

class CreateProfiles < ActiveRecord::Migration[7.0]
  def change
    create_table :profiles do |t|
      t.references :account, null: false, foreign_key: true, index: true
      t.string :name
      t.jsonb :preferences, null: false, default: {}

      t.timestamps
    end
  end
end