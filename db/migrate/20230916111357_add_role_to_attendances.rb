# frozen_string_literal: true

class AddRoleToAttendances < ActiveRecord::Migration[7.0]
  def change
    add_column :attendances, :role, :integer, null: false, default: 1
  end
end