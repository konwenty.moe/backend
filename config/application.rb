# frozen_string_literal: true

require_relative "boot"

require "rails/all"

Bundler.require(*Rails.groups)

module App
  class Application < Rails::Application
    config.middleware.use Rack::MethodOverride
    config.middleware.use ActionDispatch::Flash
    config.middleware.use ActionDispatch::Cookies
    config.middleware.use ActionDispatch::Session::CookieStore

    config.load_defaults 7.0

    config.api_only = true

    config.generators do |g|
      g.test_framework :minitest, fixture: false
    end

    config.active_job.queue_adapter = :good_job
    config.good_job = {
      active_record_parent_class: "ApplicationRecord",
      enable_cron: true,
      max_threads: 4,
      poll_interval: 30,
      cron: {
        refresh_convention_searches_materialized_view: {
          cron: "every hour",
          class: "RefreshConventionSearchesMaterializedViewJob",
          args: [],
          kwargs: {},
          description: "Refresh the materialized view powering search"
        }
      }
    }
  end
end