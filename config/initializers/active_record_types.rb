# frozen_string_literal: true

require_relative "../../lib/database/types/indifferent_jsonb"

ActiveRecord::Type.register(:ind_jsonb, Database::Types::IndifferentJsonb)