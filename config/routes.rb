# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => "/api-docs"
  mount Rswag::Api::Engine => "/api-docs"
  mount GoodJob::Engine => "good_job"
  namespace :api do
    namespace :v1 do
      resources :attendances, only: %i[create]
      resources :conventions, only: %i[index show create update destroy] do
        collection do
          get "search/:query", to: "conventions#search"
        end
      end
      resources :profiles, only: %i[show]
    end
  end
  get "/up", to: "healthcheck#up"
end