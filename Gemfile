# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.3.2"

gem "argon2", "~> 2.2"
gem "bootsnap", require: false
gem "dry-monads"
gem "dry-schema"
gem "dry-transaction"
gem "dry-types"
gem "dry-validation"
gem "good_job", "~> 3.19"
gem "json_schemer"
gem "jwt", "~> 2.7"
gem "kamal", "~> 0.16.1"
gem "panko_serializer"
gem "pg", "~> 1.1"
gem "pg_search", "~> 2.3"
gem "puma"
gem "rack-cors"
gem "rails", "~> 7.1"
gem "rodauth-rails", "~> 1.8"
gem "rswag-api"
gem "rswag-ui"
gem "scenic"
gem "sentry-rails"
gem "sentry-ruby"
gem "tzinfo-data"

group :development, :test do
  gem "debug", platforms: %i[mri mingw x64_mingw]
  gem "factory_bot"
  gem "factory_bot_rails"
  gem "factory_trace"
  gem "pg_query"
  gem "prosopite"
  gem "pry"
  gem "pry-byebug"
  gem "pry-rails"
  gem "relaxed-rubocop", require: false
  gem "rspec-rails"
  gem "rswag-specs"
  gem "rubocop", require: false
  gem "rubocop-factory_bot", require: false
  gem "rubocop-minitest", require: false
  gem "rubocop-performance", require: false
  gem "rubocop-rails", require: false
  gem "rubocop-rspec", require: false
  gem "rubocop-rspec_rails", require: false
  gem "rubocop-thread_safety", require: false
end

group :development do
  gem "lefthook"
  gem "ruby-lsp-rails"
  gem "ruby-lsp-rspec", require: false
end

gem "pundit", "~> 2.3"
