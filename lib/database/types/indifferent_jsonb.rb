# frozen_string_literal: true

require "active_record/connection_adapters/postgresql_adapter"

module Database
  module Types
    # Extends Rails' Jsonb data type to deserialize it into indifferent access Hash.
    #
    # Example:
    #
    #   class SomeModel < ApplicationRecord
    #     # some_model.a_field is of type `jsonb`
    #     attribute :a_field, :ind_jsonb
    #   end
    class IndifferentJsonb < ::ActiveRecord::ConnectionAdapters::PostgreSQL::OID::Jsonb
      def type
        :ind_jsonb
      end

      def deserialize(value)
        data = super
        return unless data

        deep_indifferent_access(data)
      end

      private

      def deep_indifferent_access(data)
        case data
        when Array
          data.map { |item| deep_indifferent_access(item) }
        when Hash
          data.with_indifferent_access
        else
          data
        end
      end
    end
  end
end