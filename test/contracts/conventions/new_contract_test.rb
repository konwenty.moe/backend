# frozen_string_literal: true

require "test_helper"

module Conventions
  class NewContractTest < ActiveSupport::TestCase
    setup do
      @contract = Conventions::NewContract.new
    end

    test "it should fail with no name" do
      result = @contract.call(name: nil)

      assert(result.error?(:name))
    end

    test "it should fail with empty name" do
      result = @contract.call(name: "")

      assert(result.error?(:name))
    end

    test "it should fail with no start date" do
      result = @contract.call(start_date: nil)

      assert(result.error?(:start_date))
    end

    test "it should fail with invalid start date" do
      result = @contract.call(start_date: "2023-22-33")

      assert(result.error?(:start_date))
    end

    test "it should fail with no end date" do
      result = @contract.call(end_date: nil)

      assert(result.error?(:end_date))
    end

    test "it should fail with invalid end date" do
      result = @contract.call(end_date: nil)

      assert(result.error?(:end_date))
    end

    test "it should fail with empty city" do
      result = @contract.call(city: "")

      assert(result.error?(:city))
    end

    test "it should fail with empty address" do
      result = @contract.call(address: "")

      assert(result.error?(:address))
    end

    test "it should fail with empty socials" do
      result = @contract.call(socials: nil)

      assert(result.error?(:socials))
    end

    test "it should fail with invalid socials" do
      result = @contract.call(socials: {})

      assert(result.error?(:socials))
    end

    test "it should fail with socials not conforming to schema" do
      result = @contract.call(socials: [{ test: "field" }])

      assert(result.error?(:socials))
    end

    test "it should fail with start date > end date" do
      result = @contract.call(start_date: "2022-10-01", end_date: "2022-09-30")
      errors = result.errors.to_h

      assert(result.error?(:start_date))
      assert(result.error?(:end_date))
      assert_match(/must be before end date/, errors[:start_date].first)
      assert_match(/must be after start date/, errors[:end_date].first)
    end
  end
end