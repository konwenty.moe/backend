# frozen_string_literal: true

require "test_helper"

module Conventions
  class UpdateTest < ActiveSupport::TestCase
    setup do
      @existing_convention = FactoryBot.create(:convention)
      @service = Conventions::Update.new

      user = @existing_convention.created_by
      @new_values = {
        id: @existing_convention.id,
        name: "New name",
        start_date: (@existing_convention.start_date + 1.day).to_s,
        end_date: (@existing_convention.end_date + 2.days).to_s
      }.freeze

      @input = [user, @new_values].freeze

      @expected_result = FactoryBot.build_stubbed(:convention,
                                                  name: @new_values[:name],
                                                  start_date: Date.parse(@new_values[:start_date]),
                                                  end_date: Date.parse(@new_values[:end_date]))
    end

    test "it updates an existing Convention convention" do
      result = @service.call(@input)

      updated_convention = result.value!

      assert_equal updated_convention.name, @new_values[:name]
      assert_equal updated_convention.start_date.to_s, @new_values[:start_date]
      assert_equal updated_convention.end_date.to_s, @new_values[:end_date]
    end
  end
end