# frozen_string_literal: true

require "test_helper"

module Conventions
  class NewTest < ActiveSupport::TestCase
    setup do
      @expected_result = FactoryBot.build(:convention)
      @service = Conventions::New.new
      user = FactoryBot.create(:account)
      params = @expected_result.attributes.except(*%w[
                                                    id
                                                    start_date
                                                    end_date
                                                    created_at
                                                    updated_at
                                                  ]).merge(
                                                    start_date: @expected_result.start_date.to_s,
                                                    end_date: @expected_result.end_date.to_s
                                                  )
      @input = [user, params].freeze
    end

    test "it creates a new Convention object" do
      Convention.stub(:new, @expected_result) do
        result = @service.call(@input)

        assert_equal Success(@expected_result), result
      end
    end
  end
end