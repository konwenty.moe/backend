# frozen_string_literal: true

require "test_helper"

module Conventions
  class ListTest < ActiveSupport::TestCase
    setup do
      FactoryBot.create(:convention, start_date: "2023-03-19", end_date: "2023-03-20")
      FactoryBot.create(:convention, start_date: "2023-03-12", end_date: "2023-03-15")
      FactoryBot.create(:convention, start_date: "2023-03-21", end_date: "2023-03-25")
      FactoryBot.create(:convention, start_date: "2023-04-13", end_date: "2023-04-16")
      @conventions = Convention.all
      @service = Conventions::List.new
    end

    test "it should return all Conventions" do
      result = @service.call
      expected_result = @conventions.sort_by(&:start_date)
      assert_equal result, Success(expected_result)
    end
  end
end