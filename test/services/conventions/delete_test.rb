# frozen_string_literal: true

require "test_helper"

module Conventions
  class DeleteTest < ActiveSupport::TestCase
    setup do
      @convention = FactoryBot.create(:convention)
      @service = Conventions::Delete.new

      user = @convention.created_by
      params = {
        id: @convention.id
      }
      @input = [user, params].freeze
    end

    test "it removes a Convention" do
      Convention.stub(:find_by, @convention) do
        result = @service.call(@input)

        assert_equal Success(), result
      end
    end
  end
end
