# frozen_string_literal: true

FactoryBot.define do
  factory :convention do
    name { "Test convention" }
    city { "Test city" }
    address { "Test address" }
    start_date { "2022-10-30 18:44:27" }
    end_date { "2022-10-30 18:44:27" }
    description { "Some description" }
    socials { [] }
    created_by { association :account }
  end
end