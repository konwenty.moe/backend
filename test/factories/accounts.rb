# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    sequence(:email) { |n| "user#{n}@example.org" }
    password { "super_secret_password" }

    traits_for_enum :status

    trait :with_profile do
      after(:create) do |account, _context|
        create(:profile, account:)
      end
    end
  end
end