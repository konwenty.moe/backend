# frozen_string_literal: true

FactoryBot.define do
  factory :attendance do
    convention { association :convention }
    profile { association :profile }
    traits_for_enum :visibility
    traits_for_enum :role
  end
end