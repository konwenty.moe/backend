# frozen_string_literal: true

FactoryBot.define do
  factory :profile do
    account { association :account }
    name { "Super Konwentowicz" }
  end
end