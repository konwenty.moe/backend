# frozen_string_literal: true

require "test_helper"

class AttendanceTest < ActiveSupport::TestCase
  setup do
    @convention = FactoryBot.build_stubbed(:convention)
    @account = FactoryBot.build_stubbed(:account)
    @profile = FactoryBot.build_stubbed(:profile, account: @account)
  end

  test "it should not be valid without Convention" do
    attendance = FactoryBot.build_stubbed(:attendance, convention: nil, profile: @profile)
    assert_not attendance.valid?
    assert_equal ["must exist"], attendance.errors[:convention]
  end

  test "it should not be valid without Profile" do
    attendance = FactoryBot.build_stubbed(:attendance, convention: @convention, profile: nil)
    assert_not attendance.valid?
    assert_equal ["must exist"], attendance.errors[:profile]
  end

  test "it must have a visibility" do
    attendance = FactoryBot.build_stubbed(:attendance, convention: @convention, profile: @profile, visibility: nil)
    assert_not attendance.valid?
    assert_equal ["can't be blank"], attendance.errors[:visibility]
  end

  test "it must have a role" do
    attendance = FactoryBot.build_stubbed(:attendance, convention: @convention, profile: @profile, role: nil)
    assert_not attendance.valid?
    assert_equal ["can't be blank"], attendance.errors[:role]
  end
end