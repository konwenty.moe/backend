# frozen_string_literal: true

require "test_helper"

class ConventionTest < ActiveSupport::TestCase
  test "should not save convention without title" do
    convention = FactoryBot.build(:convention, name: nil)
    assert_not convention.save
  end

  test "should not save convention with an empty name" do
    convention = FactoryBot.build(:convention, name: "")
    assert_not convention.save
  end

  test "should not save convention without a start date" do
    convention = FactoryBot.build(:convention, start_date: nil)
    assert_not convention.save
  end

  test "should not accept an invalid start date" do
    convention = FactoryBot.build(:convention, start_date: "2023-22-33")
    assert_not convention.save
  end

  test "should not save convention without an end date" do
    convention = FactoryBot.build(:convention, end_date: nil)
    assert_not convention.save
  end

  test "should not accept an invalid end date" do
    convention = FactoryBot.build(:convention, end_date: "2023-22-33")
    assert_not convention.save
  end
end