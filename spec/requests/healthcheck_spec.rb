# frozen_string_literal: true

require "swagger_helper"

RSpec.describe "up", type: :request do
  path "/up" do
    get("check status") do
      tags "Healthcheck"

      response(200, "application is working") do
        run_test!
      end
    end
  end
end