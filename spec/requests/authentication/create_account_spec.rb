# frozen_string_literal: true

require "swagger_helper"

RSpec.describe "/create-account", type: :request do
  path "/create-account" do
    post("create account") do
      tags "Authentication"
      parameter name: :data, in: :body, schema: {
        type: :object,
        properties: {
          login: { type: :string },
          password: { type: :string },
          "password-confirm": { type: :string },
          name: { type: :string }
        },
        required: %w[login password password-confirm name]
      }
      produces "application/json"
      consumes "application/json"

      response(200, "account creation successful") do
        let(:data) do
          {
            login: "test@test.com",
            password: "qweasdzxc",
            "password-confirm": "qweasdzxc",
            name: "Test User"
          }
        end

        run_test! do
          data = JSON.parse(response.body)
          expect(data["success"]).to match(/verify your account/)
        end
      end

      response(422, "email already taken") do
        before do
          FactoryBot.create(:account, :verified, email: "test@test.com", password: "qweasdzxc")
        end

        let(:data) do
          {
            login: "test@test.com",
            password: "qweasdzxc",
            "password-confirm": "qweasdzxc",
            name: "Test User"
          }
        end

        run_test! do
          data = JSON.parse(response.body)
          field_name = data["field-error"][0]
          error = data["field-error"][1]
          expect(field_name).to eq("login")
          expect(error).to match(/already an account/)
        end
      end

      response(422, "passwords don't match") do
        let(:data) do
          {
            login: "test@test.com",
            password: "qweasdzxc",
            "password-confirm": "qweasdzxc1",
            name: "Test User"
          }
        end

        run_test! do
          data = JSON.parse(response.body)
          field_name = data["field-error"][0]
          error = data["field-error"][1]
          expect(field_name).to eq("password")
          expect(error).to match(/passwords do not match/)
        end
      end
    end
  end
end