# frozen_string_literal: true

require "swagger_helper"

RSpec.describe "/login", type: :request do
  path "/login" do
    post("login") do
      tags "Authentication"
      parameter name: :data, in: :body, schema: {
        type: :object,
        properties: {
          login: { type: :string },
          password: { type: :string }
        },
        required: %w[login password]
      }
      produces "application/json"
      consumes "application/json"

      response(401, "wrong login or password") do
        let(:data) do
          {
            login: "non-existing@user.org",
            password: "password"
          }
        end

        run_test!
      end

      response(403, "account inactive") do
        before do
          FactoryBot.create(:account, :unverified, email: "test@test.com", password: "qweasdzxc")
        end

        let(:data) do
          {
            login: "test@test.com",
            password: "qweasdzxc"
          }
        end

        run_test! do
          data = JSON.parse(response.body)
          expect(data["error"]).to match(/awaiting verification/)
        end
      end

      response(200, "returns JWT token") do
        before do
          FactoryBot.create(:account, :verified, email: "test@test.com", password: "qweasdzxc")
        end

        let(:data) do
          {
            login: "test@test.com",
            password: "qweasdzxc"
          }
        end

        run_test! do
          jwt = response.headers["Authorization"]
          expect(jwt).not_to be_nil

          data = JSON.parse(response.body)
          expect(data["success"]).to eq("You have been logged in")
        end
      end
    end
  end
end