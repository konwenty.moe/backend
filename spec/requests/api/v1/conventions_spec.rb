# frozen_string_literal: true

require "swagger_helper"

RSpec.describe "api/v1/conventions", type: :request do
  before do
    @account = FactoryBot.create(:account, :verified, email: "test@test.com", password: "qweasdzxc")
    FactoryBot.create(:convention, start_date: "2023-01-01")
    FactoryBot.create(:convention, start_date: "2023-03-01")
    FactoryBot.create(:convention, start_date: "2023-04-02")
    @other_convention = FactoryBot.create(:convention, start_date: "2023-01-07")
    @convention = FactoryBot.create(:convention, name: "łóżcon", city: "Wrocław", created_by: @account)
    Convention.refresh_materialized_view
    post "/login", as: :json, params: { login: @account.email, password: @account.password }
    @jwt_token = @response.headers["Authorization"]
  end

  path "/api/v1/conventions" do
    get("list conventions") do
      tags "Conventions"
      response(200, "Returns a list of all Conventions") do
        run_test! do |response|
          data = JSON.parse(response.body)
          dates = data.map { |h| Date.parse(h["start_date"]) }
          expect(dates.first).to be < dates.last
        end
      end
    end

    post("create convention") do
      tags "Conventions"
      security [
        bearer_auth: []
      ]
      parameter name: :convention, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          city: { type: :string },
          address: { type: :string },
          start_date: { type: :string },
          end_date: { type: :string },
          description: { type: :string },
          socials: {
            type: :array,
            items: {
              type: :object,
              schema: {
                name: { type: :string },
                url: {
                  type: :string,
                  pattern: "^https?://"
                }
              }
            }
          }
        },
        required: %w[name city address start_date end_date]
      }
      produces "application/json"
      consumes "application/json"
      request_body_example value: {
        name: "Test convention",
        start_date: "2023-05-04",
        end_date: "2023-05-06"
      }, summary: "Valid request"
      request_body_example value: {
        name: "Test convention",
      }, summary: "Invalid request"

      response "201", "Convention successfuly created" do
        let(:Authorization) { "Bearer #{@jwt_token}" }
        let(:convention) {
          {
            name: "Test convention",
            city: "Somewhere",
            address: "Some address",
            start_date: "2023-05-04",
            end_date: "2023-05-06",
            socials: [{ name: "Facebook", url: "https://facebook.com" }]
          }
        }

        run_test!
      end

      response "422", "Error when creating convention" do
        let(:Authorization) { "Bearer #{@jwt_token}" }
        context "when name is missing" do
          let(:convention) {
            {
              start_date: "2023-05-04",
              end_date: "2023-05-06"
            }
          }

          run_test!
        end

        context "when start_date is missing" do
          let(:convention) {
            {
              name: "Test convention",
              end_date: "2023-05-06"
            }
          }

          run_test!
        end

        context "when end_date is missing" do
          let(:convention) {
            {
              name: "Test convention",
              start_date: "2023-05-04",
            }
          }

          run_test!
        end
      end

      response(400, "Invalid JWT") do
        let(:Authorization) {}
        let(:convention) {}

        run_test!
      end
    end
  end

  path "/api/v1/conventions/{convention_id}" do
    parameter name: "convention_id", in: :path, type: :integer, description: "Convention ID"

    get("show convention") do
      tags "Conventions"

      response(200, "existing convention") do
        let(:convention_id) { @convention.id }

        run_test!
      end

      response(404, "nonexistent convention") do
        let(:convention_id) { Convention.last&.id&.+ 1 || 99_999 }

        run_test!
      end
    end

    patch("update convention") do
      tags "Conventions"
      security [
        bearer_auth: []
      ]
      parameter name: :convention, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          city: { type: :string },
          address: { type: :string },
          start_date: { type: :string },
          end_date: { type: :string },
          description: { type: :string },
          socials: {
            type: :array,
            items: {
              type: :object,
              schema: {
                name: { type: :string },
                url: {
                  type: :string,
                  pattern: "^https?://"
                }
              }
            }
          }
        }
      }
      produces "application/json"
      consumes "application/json"
      request_body_example value: {
        name: "New convention name",
        start_date: "2023-05-06",
        end_date: "2023-05-07"
      }, summary: "Valid request"
      request_body_example value: {
        start_date: "2023-05-03",
        end_date: "2023-05-01"
      }, summary: "Invalid request"

      let(:convention_id) { @convention.id }

      response(200, "Convention successfully updated") do
        let(:Authorization) { "Bearer #{@jwt_token}" }
        let(:convention) {
          {
            name: "New name"
          }
        }

        run_test! do |response|
          data = JSON.parse(response.body)
          expect(data["name"]).to eq("New name")
        end
      end

      response(403, "Unauthorized") do
        let(:Authorization) { "Bearer #{@jwt_token}" }
        let(:convention_id) { @other_convention.id }
        let(:convention) {
          {
            name: "New name"
          }
        }

        run_test!
      end

      response(422, "Error when updating convention") do
        let(:Authorization) { "Bearer #{@jwt_token}" }
        let(:convention) {
          {
            start_date: "2023-05-03",
            end_date: "2023-05-01"
          }
        }

        run_test!
      end

      response(404, "Convention not found") do
        let(:Authorization) { "Bearer #{@jwt_token}" }
        let(:convention_id) { Convention.last&.id&.+ 1 || 99_999 }
        let(:convention) {
          {
            start_date: "2023-05-01",
            end_date: "2023-05-03"
          }
        }

        run_test!
      end

      response(400, "Invalid JWT") do
        let(:Authorization) {}
        let(:convention) {}

        run_test!
      end
    end

    delete("delete convention") do
      tags "Conventions"
      security [
        bearer_auth: []
      ]

      response(200, "Convention successfully deleted") do
        let(:Authorization) { "Bearer #{@jwt_token}" }
        let(:convention_id) { @convention.id }

        run_test!
      end

      response(403, "Unauthorized to delete convention") do
        let(:Authorization) { "Bearer #{@jwt_token}" }
        let(:convention_id) { @other_convention.id }

        run_test!
      end

      response(404, "Convention not found") do
        let(:Authorization) { "Bearer #{@jwt_token}" }
        let(:convention_id) { Convention.last&.id&.+ 1 || 99_999 }

        run_test!
      end
    end
  end

  path "/api/v1/conventions/search/{query}" do
    parameter name: "query", in: :path, type: :string, description: "keywords to search for"

    get("search conventions") do
      tags "Conventions"

      response(200, "search successful") do
        let(:query) { URI.encode_uri_component("lozcon wroclaw") }

        run_test! do |response|
          data = JSON.parse(response.body)
          expect(data.first["id"]).to eq @convention.id
        end
      end
    end
  end
end