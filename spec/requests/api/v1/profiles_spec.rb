# frozen_string_literal: true

require "swagger_helper"

RSpec.describe "api/v1/profiles", type: :request do
  path "/api/v1/profiles/{id}" do
    parameter name: "id", in: :path, type: :integer

    get("show user's profile") do
      tags "Profiles"
      produces "application/json"
      consumes "application/json"

      response(200, "existing profile") do
        before do
          @profile = FactoryBot.create(:profile)
          FactoryBot.create(:attendance, :public, profile: @profile)
          FactoryBot.create(:attendance, :private, profile: @profile)
        end

        let(:id) { @profile.id }

        run_test! do
          data = JSON.parse(response.body)
          expected_keys = %w[id name created_at updated_at attended_conventions]
          expect(data.keys).to eq(expected_keys)
          expect(data["attended_conventions"].count).to eq(1)
        end
      end

      response(404, "missing profile") do
        let(:id) { 2137 }

        run_test!
      end
    end
  end
end