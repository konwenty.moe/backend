# frozen_string_literal: true

require "swagger_helper"

RSpec.describe "api/v1/attendances", type: :request do
  path "/api/v1/attendances" do
    post("create attendance record") do
      tags "Attendances"
      parameter name: :attendance,
                in: :body,
                required: true,
                schema: {
                  type: :object,
                  properties: {
                    convention_id: { type: :integer },
                    profile_id: { type: :integer },
                    visibility: { type: :string, enum: ::Attendance.visibilities.keys, default: "public" },
                    role: { type: :string, enum: ::Attendance.roles.keys, default: "participant" },
                  },
                  required: %w[convention_id profile_id]
                }
      produces "application/json"
      consumes "application/json"
      request_body_example value: {
        convention_id: 1,
        profile_id: 1
      }, summary: "Visiblity not specified (will be public), role not specified (will be participant)"
      request_body_example value: {
        convention_id: 1,
        profile_id: 1,
        visibility: "private"
      }, summary: "Visiblity specified"
      request_body_example value: {
        convention_id: 1,
        profile_id: 1,
        role: "helper"
      }, summary: "Role specified"
      request_body_example value: {
        convention_id: 1,
        profile_id: 1,
        visibility: "test"
      }, summary: "Invalid visibility specified"

      response(201, "Attendance record created successfuly", focus: true) do
        let(:convention) { FactoryBot.create(:convention) }
        let(:profile) { FactoryBot.create(:profile) }
        let(:attendance) {
          {
            convention_id: convention.id,
            profile_id: profile.id,
            visibility: "private",
            role: "helper"
          }
        }

        run_test!
      end

      response(422, "Invalid params") do
        context "when profile_id is missing" do
          let(:attendance) {
            {
              convention_id: 2137,
            }
          }

          run_test!
        end

        context "when convention_id is missing" do
          let(:attendance) {
            {
              profile_id: 2137,
            }
          }

          run_test!
        end

        context "when either Convetion or Profile does not exist" do
          let(:attendance) {
            {
              convention_id: 2137,
              profile_id: 2137,
            }
          }

          run_test!
        end

        context "when invalid role is given" do
          let(:expected_error) { "must be one of: #{::Attendance.roles.keys.join(', ')}" }
          let(:attendance) {
            {
              convention_id: 2137,
              profile_id: 2137,
              role: "test"
            }
          }

          run_test! do |response|
            data = JSON.parse(response.body)
            expect(data["role"]).to eq([expected_error])
          end
        end

        context "when invalid visibility level is given" do
          let(:expected_error) { "must be one of: #{::Attendance.visibilities.keys.join(', ')}" }
          let(:attendance) {
            {
              convention_id: 2137,
              profile_id: 2137,
              visibility: "test"
            }
          }

          run_test! do |response|
            data = JSON.parse(response.body)
            expect(data["visibility"]).to eq([expected_error])
          end
        end
      end
    end
  end
end